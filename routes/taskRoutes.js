let express = require('express');

let router = express.Router();

//let User = require('./../models/User'); 

let userController = require('./../controllers/userControllers');

//insert a new task


router.post("/add-user", (req, res)=>{
	userController.createUser(req.body).then(result => res.send (result));
});

router.get("/users", (req, res)=>{
	userController.retrieveUsers().then(result => res.send(result));
});


//retrieve a specific user
router.get("/users/:id", (req, res)=>{
	// console.log(req.params);
	let params = req.params.id;
	userController.retSpecificUser(params).then(result => res.send(result));
	//model.method
});

//update user's info
router.put("/users/:id", (req, res)=>{
	let params = req.params.id;
	userController.updateUser(params, req.body, {new: true}).then(result => res.send(result));
});

//delete user
router.delete("/users/:id", (req, res)=>{

	let params = req.params.id;
	// model.method
	userController.deleteUser(params).then(result => res.send(result));
	
});


module.exports = router;

