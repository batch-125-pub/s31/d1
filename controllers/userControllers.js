const User = require('./../models/User');//capturing the User Schema from User.js

//insert a new user

let createUser = function(reqBody){
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		userName: reqBody.userName,
		password: reqBody.password
	});

	let returnVal = newUser.save().then((savedUser, error)=>{
		if(error){
			return error;
		} else {
			return savedUser;
		}
	});

	return returnVal;
}

module.exports = createUser;

//retrieve all users
let retrieveUsers = function(){
	let users = User.find({}, (error, records)=>{
		if(error){
			return error;
		} else {
			return records;
		}
	});

	return retrieveUsers;
}

module.exports = retrieveUsers;

//retrieve a specific user

let retSpecificUser = (id) => {

	let specificUser = User.findById(params, (result, error)=>{
		if(error){
			return error;
		} else {
			//send document as a response
			return result;
		}
	});

	return specificUser;
}

module.exports = retSpecificUser;

//update user's info
let updateUser = (params, reqBody, options) =>{
	let updatedUser = User.findByIdAndUpdate(params, reqBody, options, (error, result)=>{
			if(error){
				return error;
			} else {
				return result;
			}
	});

	return updatedUser;
}

module.exports = updateUser;

//delete user

let deleteUser = (id) => {

	let userDeleted = User.findByIdAndDelete(id, (error, result)=>{
		if(error){
			return error;
		} else {
			//send a response true if deleted
			return true;
		}
	});

	return userDeleted;
}

module.exports = deleteUser;



